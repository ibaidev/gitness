# Gitness

1. Install dependencies.

    ```bash
    apt update && apt install -y git curl
    ```

2. Init script.

    ```bash
    sh <(curl -s https://gitlab.com/ibaidev/gitness/-/raw/main/gitness.sh) init
    ```
