#!/bin/sh
#
#    Copyright 2022 Ibai Roman
#
#    This file is part of Gitness.
#
#    Gitness is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Gitness is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Gitness. If not, see <http://www.gnu.org/licenses/>.

[ "$_DEBUG" = "on" ] && set -x

error() {
    printf "\033[1;31merror: %s\033[0m\n" "$1" >&2
}

warning() {
    printf "\033[0;33mwarning: %s\033[0m\n" "$1" >&2
}

info() {
    printf "> \033[0;36m%s\033[0m\n" "$1"
}

is_empty() {
    [ -z "$1" ]
}

not_empty() {
    [ -n "$1" ]
}

is_not_installed() {
    [ ! -x "$(command -v "$1")" ]
}

relative_path() {
    SRC_PATH=$(realpath "$1")
    TRG_PATH=$(realpath "$2")
    SRC_PATH=${SRC_PATH%/}/
    TRG_PATH=${TRG_PATH%/}/

    if [ "$SRC_PATH" = "$TRG_PATH" ]; then
        echo "."
        return
    fi

    SRC_DIRNAME=${SRC_PATH%%/*}
    TRG_DIRNAME=${TRG_PATH%%/*}
    while [ "$SRC_DIRNAME" = "$TRG_DIRNAME" ]; do
        SRC_PATH=${SRC_PATH#*/}
        TRG_PATH=${TRG_PATH#*/}
        SRC_DIRNAME=${SRC_PATH%%/*}
        TRG_DIRNAME=${TRG_PATH%%/*}
    done

    while [ "$SRC_PATH" != "$SRC_DIRNAME" ]; do
        SRC_PATH=${SRC_PATH#*/}
        TRG_PATH=../$TRG_PATH
        SRC_DIRNAME=${SRC_PATH%%/*}
    done
    echo "${TRG_PATH%/}"
}

condense_path() {
    SRC_PATH=$1
    TRG_PATH=$2/

    if [ "$TRG_PATH" = "./" ]; then
        echo "$SRC_PATH"
        return
    fi

    while [ "${TRG_PATH%%/*}" = ".." ]; do
        SRC_PATH=${SRC_PATH%/*}
        TRG_PATH=${TRG_PATH#*/}
    done
    TRG_PATH=$SRC_PATH/$TRG_PATH
    echo "${TRG_PATH%/}"
}

repo_git() {
    git -c color.ui=always -c core.pager=cat "--git-dir=$GIT_DIR" "$@"
}

sequentially() {
    # FD=$1
    # THROTTLING=$2

    if is_empty "$1" || is_empty "$2"; then
        shift 2
        "$@"
        return
    fi

    FD=$1

    flock -x "$FD"
    sleep "$2"
    shift 2
    "$@"
    flock -u "$FD"
}

mutex_print() {
    sequentially "$PRINT_FD" 0.2 "$@"
}

mutex_remote() {
    sequentially "$REMOTE_FD" "$THROTTLING" "$@"
}

version_control_file() {
    # TMP_GIT_DIR=$1
    # COMMIT_FILE=$2
    # COMMIT_MSG=$3

    if is_empty "$1"; then
        warning "$2 not version controlled."
        return
    fi

    if [ ! -d "$1" ]; then
        warning "$2 not version controlled. $1 does not exist."
        return
    fi

    git "--git-dir=$1" add "$2"

    if is_empty "$(git --git-dir="$1" config --get user.name)"; then
        warning "user.name not found in git config"
        return
    fi

    if is_empty "$(git --git-dir="$1" config --get user.email)"; then
        warning "user.email not found in git config"
        return
    fi

    git "--git-dir=$1" commit -m "$3"
}

create_git() {
    SUBCOMMAND=$1
    shift

    if [ "$SUBCOMMAND" = "clone" ]; then
        if [ "$REPO_TYPE" = "bare" ]; then
            set -- "--mirror" "$@"
        else
            set -- "--recurse-submodules" "$@"
        fi
    fi

    case "$REPO_TYPE" in
        "bare" )
            set -- "--bare" "$@" "$GIT_DIR" ;;
        "fakeBare" )
            set -- "--bare" "$@" "$GIT_DIR";;
        "default" )
            set -- "$@" "$WORK_TREE" ;;
        "separate" )
            set -- "--separate-git-dir=$GIT_DIR" "$@" "$WORK_TREE" ;;
        * )
            error "Repo type not found"
            return ;;
    esac

    git "$SUBCOMMAND" "$@"
}

create_repo() {
    info "clone"
    mkdir -p "${GIT_DIR%/*}"
    if ! is_empty "$REMOTE"; then
        mutex_remote create_git clone "$REMOTE"
    elif [ -f "$BACKUP_FILE" ]; then
        create_git clone "$BACKUP_FILE"
        repo_git remote remove origin
    else
        create_git init
        repo_git branch -m main
    fi

    if [ -f "$WORK_TREE/.git" ]; then
        rm -f "$WORK_TREE/.git"
        REL_GIT_DIR=$(relative_path "$WORK_TREE" "$GIT_DIR")
        printf "gitdir: %s\n" "$REL_GIT_DIR" > "$WORK_TREE/.git"
    fi
}

config_repo() {
    info "config"
    WRITE_CONFIG="$GIT_DIR/config"
    if [ ! -f "$WRITE_CONFIG" ]; then
        error "$WRITE_CONFIG does not exist"
        return
    fi

    git config -f "$WRITE_CONFIG" push.default simple
    git config -f "$WRITE_CONFIG" push.recurseSubmodules on-demand
    git config -f "$WRITE_CONFIG" pull.rebase false
    git config -f "$WRITE_CONFIG" diff.submodule log
    git config -f "$WRITE_CONFIG" status.submodulesummary true
    git config -f "$WRITE_CONFIG" submodule.recurse true
    if [ "$REPO_TYPE" = "bare" ]; then
        git config -f "$WRITE_CONFIG" core.bare true
    else
        git config -f "$WRITE_CONFIG" core.bare false
    fi

    git config -f "$WRITE_CONFIG" gitness.repoName "$REPO_NAME"
    git config -f "$WRITE_CONFIG" gitness.repoType "$REPO_TYPE"
    REL_GIT_DIR=${GIT_DIR#"$HOME/"}
    git config -f "$WRITE_CONFIG" gitness.gitDir "$REL_GIT_DIR"
    if [ "$REPO_TYPE" != "bare" ]; then
        REL_WORK_TREE=$(relative_path "$GIT_DIR" "$WORK_TREE")
        git config -f "$WRITE_CONFIG" core.worktree "$REL_WORK_TREE"
    fi
    if ! is_empty "$THROTTLING"; then
        git config -f "$WRITE_CONFIG" gitness.throttling "$THROTTLING"
    fi
    if ! is_empty "$BACKUP_FILE"; then
        REL_BACKUP_FILE=${BACKUP_FILE#"$HOME/"}
        git config -f "$WRITE_CONFIG" gitness.backupFile "$REL_BACKUP_FILE"
    fi
    if [ "$REPO_TYPE" = "fakeBare" ]; then
        if ! is_empty "$REMOTE"; then
            git config -f "$WRITE_CONFIG" --add remote.origin.fetch \
                "+refs/heads/*:refs/remotes/origin/*"
        fi
        git config -f "$WRITE_CONFIG" status.showUntrackedFiles no
        REPO_ID=${GIT_DIR##*/}
        REPO_ID=${REPO_ID%.*}
        git config --global "alias.ness-$REPO_ID" \
            "!git --git-dir=\$HOME/$REL_GIT_DIR"
    fi
}

check_repo() {
    if is_empty "$WORK_TREE"; then
        return
    fi

    info "check"
    STATUS="$(repo_git status --porcelain=v2 --branch --show-stash 2>/dev/null)"
    DIRTY="$(echo "$STATUS" | \
        awk 'BEGIN { ORS=" " }; $1 !~ /[#?]/ { print $9 }')"
    if not_empty "$DIRTY"; then
        printf "\033[0;33mdirty: %s\t" "$DIRTY"
    fi
    UNTRACKED="$(echo "$STATUS" | \
        awk 'BEGIN { ORS=" " }; $1 == "?" { print $2 }')"
    if not_empty "$UNTRACKED"; then
        printf "\033[0;35muntracked: %s\t" "$UNTRACKED"
    fi
    REMOTE="$(echo "$STATUS" | \
        awk 'BEGIN { ORS=" " }; $1 == "#" && $2 == "branch.ab" { print $3 $4 }')"
    if not_empty "$REMOTE" && [ "$REMOTE" != "+0-0 " ]; then
        printf "\033[0;31mremote: %s\t" "$REMOTE"
    fi
    STASH="$(echo "$STATUS" | \
        awk 'BEGIN { ORS=" " }; $1 == "#" && $2 == "stash" { print $3 }')"
    if not_empty "$STASH"; then
        printf "\033[0;36mstash: %s\t" "$STASH"
    fi
    if is_empty "$DIRTY" && \
            is_empty "$UNTRACKED" && \
            (is_empty "$REMOTE" || [ "$REMOTE" = "+0-0 " ]) && \
            is_empty "$STASH"; then
        printf "\033[0;32mOK\t"
    fi
    printf "\033[0m\n"
}

pull_repo() {
    if is_empty "$REMOTE"; then
        return
    fi
    if is_empty "$WORK_TREE"; then
        return
    fi

    info "pull"
    mutex_remote repo_git pull --recurse-submodules --all
}

fetch_repo() {
    if is_empty "$REMOTE"; then
        return
    fi

    info "fetch"
    mutex_remote repo_git fetch --recurse-submodules --all
}

update_submodules() {
    if is_empty "$REMOTE"; then
        return
    fi
    if is_empty "$WORK_TREE"; then
        return
    fi

    info "update submodules"
    # TODO: remove -C if no longer required by git-submodule.
    mutex_remote repo_git -C "$WORK_TREE" submodule update --remote --init
}

backup_repo() {
    if is_empty "$BACKUP_FILE"; then
        return
    fi

    info "backup"
    mkdir -p "${BACKUP_FILE%/*}"
    repo_git bundle create "$BACKUP_FILE" --all && \
        printf "%s created\n" "$BACKUP_FILE"
}

checkout_repo() {
    CURRENT_BRANCH=$(repo_git branch --show-current)
    if is_empty "$(repo_git show-ref "refs/heads/$CURRENT_BRANCH")"; then
        return
    fi

    info "checkout"
    # TODO: From git v2.39.1 onwards it can be replaced by:
    # repo_git ls-tree --full-tree -r --format '%(objecttype),%(path)' "$CURRENT_BRANCH" |
    repo_git ls-tree --full-tree -r "$CURRENT_BRANCH" | tr " \t" "," | cut -d',' -f2,4 |
        while IFS=',' read -r OBJECT_TYPE OBJECT_PATH; do
            if [ "$OBJECT_TYPE" = "blob" ]; then
                if [ -f "$WORK_TREE/$OBJECT_PATH" ]; then
                    repo_git add "$OBJECT_PATH"
                else
                    repo_git checkout "$CURRENT_BRANCH" "$OBJECT_PATH"
                fi
            elif [ "$OBJECT_TYPE" = "commit" ]; then
                if [ ! -d "$WORK_TREE/$OBJECT_PATH" ]; then
                    repo_git reset "$CURRENT_BRANCH" -- "$OBJECT_PATH"
                    mkdir -p "$WORK_TREE/$OBJECT_PATH"
                    # TODO: remove -C if no longer required by git-submodule.
                    repo_git -C "$WORK_TREE" \
                        submodule update --init "$OBJECT_PATH"
                fi
            fi
        done
}

set_upstream() {
    CURRENT_BRANCH=$(repo_git branch --show-current)
    if is_empty "$(repo_git show-ref "refs/remotes/origin/$CURRENT_BRANCH")"; then
        return
    fi

    info "set upstream"
    repo_git branch "--set-upstream-to=origin/$CURRENT_BRANCH"
}

deploy_repo() {
    if [ -d "$GIT_DIR" ] ; then
        if [ -f "$GIT_DIR/config" ] && [ ! -f "$GIT_DIR/HEAD" ]; then
            rm -rf "$GIT_DIR"
        else
            warning "$GIT_DIR already exists."
            return;
        fi
    fi
    create_repo
    config_repo
    if [ "$REPO_TYPE" = "fakeBare" ]; then
        if ! is_empty "$REMOTE"; then
            fetch_repo
            set_upstream
            checkout_repo
        elif [ -f "$BACKUP_FILE" ]; then
            checkout_repo
        fi
    fi
    check_repo
}

do_pull() {
    pull_repo
    check_repo
}

do_update() {
    update_submodules
    check_repo
}

do_backup() {
    if [ "$REPO_TYPE" = "bare" ]; then
        fetch_repo
    else
        pull_repo
    fi
    backup_repo
}

run_git() {
    info "$1"
    repo_git "$@"
}

show_result() {
    printf "%-20.20s %s\n" \
        "$REPO_NAME" \
        "$(echo "$SHELL_OUTPUT" | tr '\n' ' ' | tr '>' '|' )"
}

manage_repo() {
    SHELL_OUTPUT=$("$@" 2>&1)

    if not_empty "$SHELL_OUTPUT"; then
        mutex_print show_result
    fi
}

read_repo_params() {
    REPO_NAME=$(git config -f "$READ_CONFIG" --get gitness.repoName)

    REPO_TYPE=$(git config -f "$READ_CONFIG" --get gitness.repoType)

    REL_GIT_DIR=$(git config -f "$READ_CONFIG" --get gitness.gitDir)
    GIT_DIR=${REL_GIT_DIR:+$HOME/$REL_GIT_DIR}

    REL_WORK_TREE=$(git config -f "$READ_CONFIG" --get core.worktree)
    WORK_TREE=${REL_WORK_TREE:+$(condense_path "$GIT_DIR" "$REL_WORK_TREE")}

    THROTTLING=$(git config -f "$READ_CONFIG" --get gitness.throttling)

    REL_BACKUP_FILE=$(git config -f "$READ_CONFIG" --get gitness.backupFile)
    BACKUP_FILE=${REL_BACKUP_FILE:+$HOME/$REL_BACKUP_FILE}

    REMOTE=$(git config -f "$READ_CONFIG" --get remote.origin.url)

    "$@"
}

with_fds(){
    REMOTE_FD=9
    PRINT_FD=8

    exec 9<> "$REMOTE_LOCK"
    exec 8<> "$PRINT_LOCK"

    "$@"

    exec 9>&-
    exec 8>&-
}

in_background(){
    while [ "$(pgrep -P $$ | wc -l)" -gt 8 ]; do
        sleep 0.2
    done
    "$@" &
}

loop_configs() {
    for READ_CONFIG in $ALL_CONFIGS; do
        "$@"
    done

    wait
}

with_lockfiles() {
    REMOTE_LOCK=$(mktemp -t "gitness_remote_lock_XXXXXX")
    PRINT_LOCK=$(mktemp -t "gitness_print_lock_XXXXXX")
    touch "$REMOTE_LOCK"
    touch "$PRINT_LOCK"

    "$@"

    rm "$REMOTE_LOCK"
    rm "$PRINT_LOCK"
}

for_all_configs() {
    if is_not_installed "flock"; then
        loop_configs "$@"
        return
    fi

    with_lockfiles loop_configs in_background with_fds "$@"
}

for_all_repos_in () {
    # REPOS_DIR=$1
    # COMMAND=$2
    # DIR_FILTER=$3

    ALL_CONFIGS=$(find "$1" \
        -type f \
        -wholename "*$3*/config" \
        -not -wholename "*/modules/*")
    COMMAND=$2
    shift 2
    [ $# -gt 0 ] && shift

    for_all_configs read_repo_params manage_repo "$COMMAND" "$@"
}

do_create() {
    deploy_repo

    version_control_file "$TRACKED_BY" "$GIT_DIR/config" \
        "Add $REPO_NAME config."
}

add_link() {
    # SHELL_TRACKED_BY=$1

    LOCAL_BIN_DIR=$HOME/.local/bin

    mkdir -p "$LOCAL_BIN_DIR"
    (cd "$LOCAL_BIN_DIR" && ln -s ../lib/gitness/gitness.sh gitness)

    version_control_file "$1" "$LOCAL_BIN_DIR/gitness" "Add Gitness link."
}

do_init() {
    # REPOS_DIR=$1

    if [ -d "$1" ]; then
        error "$1 already exists"
        return
    fi

    TRACKED_BY=$1/repo-configs.git
    while IFS=',' read -r REPO_NAME H_WORK_TREE REPO_TYPE REMOTE BACKUP_FILE; do
        GIT_DIR=$1/$REPO_NAME.git
        WORK_TREE=$HOME/$H_WORK_TREE
        manage_repo do_create
    done <<-EOT
	repo-configs,,fakeBare,,$HOME/backup/repo-configs.bundle
	dotfiles,,fakeBare,,$HOME/backup/dotfiles.bundle
	gitness,.local/lib/gitness,separate,https://gitlab.com/ibaidev/gitness.git,
	EOT

    TRACKED_BY=$1/dotfiles.git
    add_link "$TRACKED_BY"
    version_control_file "$TRACKED_BY" "$HOME/.gitconfig" "Add .gitconfig."
}

do_restore() {
    # REPOS_DIR=$1
    # CONFIG_REPO=$2
    if [ $# -lt 2 ]; then
        error "<configRepo> needed."
        return
    fi

    # TODO: mktemp -t is deprecated
    TMP_REPO=$(mktemp -d -t "gitness_tmp_repo_XXXXXX")

    git clone "$2" "$TMP_REPO"

    for_all_repos_in "$TMP_REPO" deploy_repo "repo-configs"
    for_all_repos_in "$1" deploy_repo "$@"

    rm -rf "$TMP_REPO"
}

ask_repo_params() {
    # REPOS_DIR=$1

    printf "Repo ID (group.repo) []: "
    read -r REPO_ID
    if is_empty "$REPO_ID"; then
        error "Repo ID is empty"
        return
    fi
    REPO_REL_PATH=$(echo "$REPO_ID" | tr "." "/")
    REPO_NAME_HINT=${REPO_REL_PATH##*/}
    GROUP_PATH=${REPO_REL_PATH%"$REPO_NAME_HINT"}

    GIT_DIR_HINT=$1/${GROUP_PATH}${REPO_NAME_HINT}.git
    printf "Git directory [%s]: " "$GIT_DIR_HINT"
    read -r GIT_DIR
    : "${GIT_DIR:=$GIT_DIR_HINT}"

    printf "Repo name [%s]: " "$REPO_NAME_HINT"
    read -r REPO_NAME
    : "${REPO_NAME:=$REPO_NAME_HINT}"

    WORK_TREE_HINT=$PWD/${GROUP_PATH}${REPO_NAME_HINT}
    printf "Working directory ('-' to unset) [%s]: " "$WORK_TREE_HINT"
    read -r WORK_TREE
    : "${WORK_TREE:=$WORK_TREE_HINT}"
    [ "$WORK_TREE" = "-" ] && unset WORK_TREE

    if is_empty "$WORK_TREE"; then
        REPO_TYPE="bare"
    elif [ "$GIT_DIR" = "$WORK_TREE/.git" ]; then
        REPO_TYPE="default"
    elif [ -d "$WORK_TREE" ]; then
        REPO_TYPE="fakeBare"
    else
        REPO_TYPE="separate"
    fi

    if [ "$REPO_TYPE" != "default" ] ; then
        TRACKED_BY_HINT=$1/${GROUP_PATH}repo-configs.git
        printf "Config tracked by ('-' to unset) [%s]: " "$TRACKED_BY_HINT"
        read -r TRACKED_BY
        : "${TRACKED_BY:=$TRACKED_BY_HINT}"
        [ "$TRACKED_BY" = "-" ] && unset TRACKED_BY
    fi

    printf "Remote URL directory []: "
    read -r REMOTE
    [ "$REMOTE" = "-" ] && unset REMOTE
    if ! is_empty "$REMOTE" ; then
        printf "Throttling needed in remote? (sec) []: "
        read -r THROTTLING
        [ "$THROTTLING" = "-" ] && unset THROTTLING
    fi

    BACKUP_FILE_HINT=$HOME/backup/${GROUP_PATH}${REPO_NAME_HINT}.bundle
    printf "Backup file ('-' to unset) [%s]: " "$BACKUP_FILE_HINT"
    read -r BACKUP_FILE
    : "${BACKUP_FILE:=$BACKUP_FILE_HINT}"
    [ "$BACKUP_FILE" = "-" ] && unset BACKUP_FILE

    shift

    "$@"
}

show_help() {
    cat <<-EOT
	Usage: $SCRIPT_NAME <command>

	commands:
	    init                   Init gitness.
	    create                 Create a git repo.
	    pull    [<dirFilter>]  Pull from origin and check the status.
	    update  [<dirFilter>]  Update submodules.
	    git      <dirFilter>
	             <gitCommand>  Execute a git command in the selected repos.
	    sync    [<dirFilter>]  Recreate missing repos.
	    backup  [<dirFilter>]  Backup the selected repos.
	    restore  <configRepo>  Search in a config dir and restore found repos.
	EOT
}

do_action() {
    if [ $# -eq 0 ]; then
        show_help
        return
    fi

    ACTION=$1
    shift

    is_empty "$REPOS_DIR" && REPOS_DIR="$HOME/.repos"

    case $ACTION in
        init|ini) do_init "$REPOS_DIR";;
        create|cr) ask_repo_params "$REPOS_DIR" do_create;;
        pull|pu) for_all_repos_in "$REPOS_DIR" do_pull "$@";;
        update|up) for_all_repos_in "$REPOS_DIR" do_update "$@";;
        git) for_all_repos_in "$REPOS_DIR" run_git "$@";;
        # TODO: does not destroy non tracked ones. Store TRACKED_BY?
        sync|sy) for_all_repos_in "$REPOS_DIR" deploy_repo "$@";;
        backup|bu) for_all_repos_in "$REPOS_DIR" do_backup "$@";;
        restore|res) do_restore "$REPOS_DIR" "$@";;
        *) show_help
        ;;
    esac
}

SCRIPT_NAME=${0##*/}

if is_not_installed "git"; then
    error "git is not installed."
    exit 1
fi

do_action "$@"

